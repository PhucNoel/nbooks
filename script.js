$('#search').keyup(function() {
	var searchField = $('#search').val();
	var myExp = new RegExp(searchField, "i");
	$.getJSON('data.json', function(data) {
		var output = '<ul class="media-list">';
		$.each(data, function(key, val) {
			if ((val.name.search(myExp) != -1) ||
			(val.bio.search(myExp) != -1)) {
				output += '<li class="media">';
				output += '<a class="pull-left">';
				output += '<img src="images/' + val.shortname + '.jpg" data-toggle="modal" data-target="#myModal" onclick="sendEmail(\'' + val.shortname + '\')"/>';
				output += '</a>';
				output += '<div class="media-body">';
				output += '<h4 class="media-heading">' + val.name + '</h4>';
				output += '<p>'+ val.bio +'</p>';
				output += '</div>'
				output += '</li>';
			}
		});
		output += '</ul>';
		$('#update').html(output);
	}); //get JSON
});
