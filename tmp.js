    <script>
    $( function() 
       {
            var email       = $( "#email_address" ),
                message     = $( "#message" ),
                tips        = $( ".validateTips" ),
                allFields   = $( [] ).add( email_address ).add( message );
            
            function updateTips( tip )
            {
                tips
                    .text( tip )
                    .addClass( "ui-state-highlight" );
                setTimeout( 
                                function()
                                {
                                    tips.removeClass( "ui-state-highlight", 1500 );
                                },
                                500
                          );
            }

            function checkRegexp( o, regexp, n )
            {
                if( !( regexp.test( o.val() ) ) )
                {
                    
                    o.addClass( "ui-state-error" );
                    updateTips( n );
                    
                    return false;
                }
                else
                {
                    return true;
                }
            }

            $( "#dialog-form" ).dialog({
                autoOpen: false,
                height  : 300,
                width   : 350,
                modal   : true,
                buttons:
                {
                    "Send": function()
                    {
                        var b_valid = true;
                        allFields.removeClass( "ui-state-error" );

                        b_valid = b_valid && checkRegexp( email, /^[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9-]+)*(\.[A-Za-z]{2,4})$/, "Email need to start with a letter or nuber" );

                        if( b_valid )
                        {
                            $( this ).dialog( "close" );
                        }
                    },
                    Cancel: function()
                    {
                        $( this ).dialog( "close" );
                    }
                },
                close: function()
                {
                    allFields.val( "" ).removeClass( "ui-state-error" ); 
                }
            }

            $( "#update img" )
                .click( 
                        function()
                        {
                            $( "#dialog-form" ).dialog( "open" );
                        }
                      );

        }
            
    </script>
